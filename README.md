# android_vendor_xiaomi_haydn-firmware

Firmware images for Mi 11i (haydn), to include in custom ROM builds.

**Current version**: fw_haydn_miui_HAYDNGlobal_V14.0.3.0.TKKMIXM_5c6f79b97e_13.0

### How to use?

1. Clone this repo to `vendor/xiaomi/haydn-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/haydn-firmware/BoardConfigVendor.mk
```
